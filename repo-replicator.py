#!/usr/bin/env python3

import os
import sys
import logging
import argparse
import git
import requests

from metrics import (
    prom_finish_time,
    prom_error_cnt,
    prom_synced_repos,
    push,
    write_to_textfile,
)

class Hoster:
    """
    Abstract class for the hoster.
    """
    hoster = None
    _username = None

    def get_hoster_name(self):
        """ Returns the identifier of this implementation. """
        return self.hoster

    def get_user_name(self):
        """ Returns the username for this implementation. """
        return self._username

    def get_repo_list(self):
        """ Fetches all the repositories for given user from this hoster. """
        raise Exception("not implemented")


class Github(Hoster):
    """
    Hoster implementation for github.com
    """
    hoster = "Github"

    def __init__(self, username):
        self._username = username

    def get_repo_list(self):
        response = requests.get(
            'https://api.github.com/users/{username}/repos?type="owner"&per_page=1000'.format(
                username=self._username
            )
        )

        if not response.ok:
            return tuple()

        return map(lambda x: (x["name"], x["clone_url"]), response.json())


class Gitlab(Hoster):
    """
    Hoster implementation for gitlab.com
    """
    hoster = "Gitlab"

    def __init__(self, username):
        self._username = username

    def get_repo_list(self):
        repos = list()
        proceed = True
        page = 1
        while proceed:
            response = requests.get(
                "https://gitlab.com/api/v4/users/{username}/projects?page={pagenumber}".format(
                    username=self._username, pagenumber=page
                )
            )

            if not response.ok:
                return tuple()

            page += 1
            fetched = list(map(lambda x: (x["path"], x["http_url_to_repo"]), response.json()))
            if fetched:
                repos.extend(fetched)
            else:
                proceed = False
        return repos


class RepoFetcher:
    def __init__(self, hoster, args):
        if not hoster:
            raise ValueError("No hoster supplied")
        self._hoster = hoster

        if not args:
            raise ValueError("No args supplied")
        self._args = args

        self._dry_run = args.dry_run

        if not os.path.exists(args.dest):
            sys.exit("Dir '{dir}' does not exist".format(dir=args.dest))
        self._dest = args.dest

    def is_repo_already_checked_out(self, repo_name):
        """
        Checks whether the repository is already existent on disk. Returns True, if the repo
        already exists, otherwise False.
        """
        path = os.path.join(self._dest, repo_name)
        return os.path.isdir(path)

    def pull_changes(self, repo_name):
        """
        Pull the changes of the remote repository.
        """
        prom_synced_repos.labels(
            operation="pull",
            provider=self._hoster.get_hoster_name(),
            username=self._hoster.get_user_name(),
        ).inc()
        path = os.path.join(self._dest, repo_name)
        logging.info("Pulling changes for '%s'...", repo_name)
        if not self._dry_run:
            repo = git.cmd.Git(path)
            repo.pull()

    def clone_repo(self, remote_url, repo_name):
        """
        Clones a repository to the disk. Accepts a remote_url and the repo_name.
        """
        logging.info("Cloning '%s'...", repo_name)
        prom_synced_repos.labels(
            operation="clone",
            provider=self._hoster.get_hoster_name(),
            username=self._hoster.get_user_name(),
        ).inc()
        if not self._dry_run:
            git.Git(self._dest).clone(remote_url)

    def work(self):
        """
        Iterates over all the repositories and either clones or pulls the repository.
        """
        repos = None
        try:
            repos = self._hoster.get_repo_list()
            if not repos:
                logging.warning(
                    "No %s repositories found for username '%s'",
                    self._hoster.get_hoster_name(),
                    self._hoster.get_user_name(),
                )
                return
        except requests.exceptions.RequestException as error:
            logging.error(
                "Error retrieving %s repository for user %s: %s",
                self._hoster.get_hoster_name(),
                self._hoster.get_user_name(),
                error,
            )

        for repo in repos:
            repo_name = repo[0]
            remote_url = repo[1]
            try:
                if self.is_repo_already_checked_out(repo_name):
                    self.pull_changes(repo_name)
                else:
                    self.clone_repo(remote_url, repo_name)
            except Exception as error:
                prom_error_cnt.labels(
                    repo_name=repo_name,
                    provider=self._hoster.get_hoster_name(),
                    username=self._hoster.get_user_name(),
                ).inc()
                logging.error("Error occured synchronizing repo '%s': %s", repo_name, error)

        prom_finish_time.set_to_current_time()
        if self._args.pushgateway:
            push(self._args.pushgateway)
        elif self._args.prom_file:
            write_to_textfile(self._args.prom_file)


def setup_logging():
    """ Sets up the logging. """
    loglevel = logging.INFO
    logging.basicConfig(
        level=loglevel, format="%(levelname)s\t %(asctime)s %(message)s"
    )


def parse_args():
    """
    Parse all the command line arguments. Returns an initialized Namespace object containing
    all the parsed arugments.
    """
    parser = argparse.ArgumentParser(
        description="Clones / pulls changes for all accessible Github repositories of a certain user. "
    )
    subparsers = parser.add_subparsers(dest="subparser")
    parser_github = subparsers.add_parser(Github.hoster.lower(), help="Github hoster")
    parser_github.add_argument("-u", "--user", help="Github user name", required=True)

    parser_gitlab = subparsers.add_parser(Gitlab.hoster.lower(), help="Gitlab hoster")
    parser_gitlab.add_argument("-u", "--user", help="Gitlab user name", required=True)

    parser.add_argument(
        "-n",
        "--dry-run",
        help="Only simulate actions",
        action="store_true",
        default=False,
    )
    parser.add_argument(
        "-d", "--dest", help="Destination to store the repositories", required=True
    )

    prometheus = parser.add_mutually_exclusive_group()
    prometheus.add_argument(
        "-g", "--pushgateway", help="Prometheus pushgateway URL", action="store"
    )
    prometheus.add_argument(
        "-f",
        "--prom-file",
        help="Prometheus nodeexporter textfile directory",
        action="store",
    )

    return parser.parse_args()


def main():
    """ Starts the whole thing. """
    args = parse_args()
    if not args.subparser:
        sys.exit("You must specify a subcommand")

    hoster = None
    if args.subparser == Gitlab.hoster.lower():
        hoster = Gitlab(args.user)
    elif args.subparser == Github.hoster.lower():
        hoster = Github(args.user)

    fetcher = RepoFetcher(hoster, args)
    fetcher.work()


if __name__ == "__main__":
    setup_logging()
    main()
