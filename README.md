# repo-replicator
## what?
Simple python script that helps you avoid keeping github as your single point of failure. It fetches all (public) repositories of a Github user and clones / pulls the latest changes to a dedicated directory.

## why?
I don't always have every repo checked out on my (main) computer/s. I don't want my stuff that no-one else forks to be gone once github decides to shut (me) down. I keep this script running on my servers to automatically fetch my latest changes. 

## warning!
Of course you should not point the 'destination' to a directory you're actively developing your git projects or you will risk nasty git states. 

# dependencies
- python3
- requests
- git

## get deps on debian
```shell
apt install python3-git python3-requests
```

# usage:
```
usage: github-replicator.py [-h] -u USER -d DEST

Clones / pulls changes for all accessible Github repositories of a certain
user.

optional arguments:
  -h, --help            show this help message and exit

required named arguments:
  -u USER, --user USER  Github user name
  -d DEST, --dest DEST  Destination to store the repositories
```

# example:
```
>: ./github-replicator.py -u soerenschneider -d ~/github-backup/
Pulling changes for 'ansibles'...
Already up to date.

Pulling changes for 'blog'...
Already up to date.

Pulling changes for 'dotfiles'...
Already up to date.

Pulling changes for 'graylog2-server-slack-docker'...
Already up to date.

Pulling changes for 'repo-replicator'...
Updating b6b612a..257bbc4
Fast-forward
 github-replicator.py | 46 ++++++++++++++++++++++++++++++++++++++++++++++
 1 file changed, 46 insertions(+)
 create mode 100755 github-replicator.py

...
```
